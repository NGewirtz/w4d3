class SessionsController < ApplicationController

  before_action :already_logged_in, only: [:new, :create]


  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_credentials(params[:user][:username], params[:user][:password])

    if @user
      login(@user)
      redirect_to cats_url
    else
      flash.now[:errors] = ['Invalid Username/Password combination']
      render :new
    end

  end

  def destroy
    Session.find_by(session_token: session[:session_token]).destroy
    session[:session_token] = nil
    @current_user = nil
    redirect_to cats_url
  end

end
