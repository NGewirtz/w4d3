class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user, :logged_in?, :is_owner?

  def current_user
    return @current_user if @current_user
    new_session = Session.find_by(session_token: session[:session_token])
    if new_session
      @cuurrent_user = new_session.user
    else
      @current_user = nil
    end
  end

  def login(user)
    @current_user = user
    session[:session_token] = user.reset_session_token!
  end

  def logout!(user)
    render plain 'here'
  end

  def logged_in?
    !!current_user
  end

  def ensure_logged_in
    unless logged_in?
      flash[:errors] = ['Please log in']
      redirect_to new_session_url
    end
  end

  def already_logged_in
    if logged_in?
      flash[:errors] = ['Already logged in!']
      redirect_to cats_url
    end
  end

  def ensure_owner
    unless is_owner?
      flash[:errors] = ["You dont own this cat"]
      redirect_to cats_url
    end
  end

  def is_owner?
    return false unless logged_in?
    !!current_user.cats.find_by(id: params[:id])
  end

end
