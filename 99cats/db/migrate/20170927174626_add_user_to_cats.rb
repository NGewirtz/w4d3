class AddUserToCats < ActiveRecord::Migration[5.1]
  def change
    add_column :cats, :user_id, :integer

    Cat.all.each do |cat|
      cat.user_id = (1..3).to_a.sample
      cat.save!
    end

    change_column :cats, :user_id, :integer, null:false
    add_index :cats, :user_id
  end
end
