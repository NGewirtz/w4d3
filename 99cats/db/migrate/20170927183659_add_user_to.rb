class AddUserTo < ActiveRecord::Migration[5.1]
  def change
    add_column :cat_rental_requests, :user_id, :integer

    CatRentalRequest.all.each do |req|
      req.user_id = (1..3).to_a.sample
      req.save!
    end

    change_column :cat_rental_requests, :user_id, :integer, null: false
    add_index :cat_rental_requests, :user_id
  end
end
